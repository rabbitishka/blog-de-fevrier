<?php

class UserRepository {
    private $db;

    public function __construct(){
        $this->db = new Database();
        $this->db = $this->db->getBDD();
      }

/**
 * Methode permettant de récupérer un objet Users construit.
 * @param  mixed $perso  Soit l'ID du perso (int), soit le nom du perso (string)
 * @return User        Retourne un User instancié.
 */
public function getPerso($perso){
  if (is_string($perso)) {
    $sql = "SELECT * FROM users WHERE USERNAME = :perso ;";
  }else{
    $sql = "SELECT * FROM users WHERE Id_perso = :perso ;";
  }
  $requete = $this->db->prepare($sql);

  $requete->execute([':perso'=>$perso]);

  $infos = $requete->fetch(PDO::FETCH_ASSOC);

  // Une fois qu'on l'a récupéré, on modifie le tableau. En effet, les clés dans $infos ont les noms des colonnes : Id_perso, Nom_perso ... Or, pour construire un objet, la méthode hydrate a besoin des attributs, sans le "_perso". Il faut donc reconstruire le tableau, en gardant juste le début des clés : id, nom, ...

  $perso = [];
  foreach($infos as $key => $value){
    $key = explode('_',$key);
    $key = $key[0];
    $perso = array_merge($perso,[$key => $value]);
  }


  // On construit le personnage :
  $perso = new $perso['Type']($perso);

  return $perso;
}

      
}