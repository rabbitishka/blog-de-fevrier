<?php

class Database {
    const DB_HOST = "localhost";
    const DB_BASE = "blog_fevrier";
    const DB_USER = "blog_fevrier";
    const DB_PASSWORD = "blog_fevrier";
    const DB_TABLE_USERS = "users";
    const DB_TABLE_CREATE_ARTICLE = "create_article";
    const DB_TABLE_COMMENTS = "comments";
    const DB_TABLE_CATEGORIES = "categories";
    const DB_TABLE_BELONG_COMMENT = "belong_comment";
    const DB_TABLE_BELONG_CATEGORY = "belong_category";
    const DB_TABLE_AUTHORS = "authors";
    const DB_TABLE_ARTICLES = "articles";

    private $_BDD;

    public function __construct(){
        $this->connectBDD();
      }

      /**
   * Permet d'initialiser la connexion à la Base de Données
   * @return un objet PDO en cas de succès, ou un message d'erreur.
   */
  private function connectBDD(){
    try {
      $this->_BDD = new PDO("mysql:host=".self::DB_HOST.";dbname=".self::DB_BASE,self::DB_USER,self::DB_PASSWORD, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    } catch (PDOException $e) {
      die("Erreur de connexion : " . $e->getMessage());
    }
  }

  public function getBDD(){
    return $this->_BDD;
  }

  // Méthodes propres à la Base de données de manière générale :

  /**
   * Fonction qui permet de retourner tous les personnages stochés dans la Base de Données
   * @return Array Tableau contenant tous les personnages.
   */
  public function getAllUsers(){
    $sql = 'SELECT * FROM '.self::DB_TABLE_USERS;
    $requete =  $this->_BDD->query($sql);
    $resultat = $requete->fetchAll(PDO::FETCH_OBJ);

    return $resultat;
  }

  public function initialisationBDD(){
    $verif = $this->_BDD->query("SHOW TABLES LIKE '".self::DB_TABLE_USERS ."'");
    $verif = $verif->fetchAll(PDO::FETCH_ASSOC);
      if (empty($verif) || in_array(self::DB_TABLE_USERS,$verif[0])){
        $sql = "CREATE TABLE IF NOT EXISTS `users` (
            `ID` INT AUTO_INCREMENT,
            `USERNAME` VARCHAR(50) NOT NULL,
            `EMAIL` VARCHAR(50) NOT NULL,
            `PASSWORD` VARCHAR(100) NOT NULL,
            `DATETIME` DATETIME NULL,
            PRIMARY KEY (`ID`),
            UNIQUE(USERNAME),
            UNIQUE(EMAIL)
          ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;";

        $this->_BDD->query($sql);

        echo "La table ".self::DB_TABLE_USERS." a été créée.";

      }
  }

  public function suppressionTable(){
    $verif = $this->_BDD->query("SHOW TABLES LIKE '".self::DB_TABLE_USERS ."'");
    $verif = $verif->fetchAll(PDO::FETCH_ASSOC);

    if (!empty($verif) && in_array(self::DB_TABLE_USERS,$verif[0])){
      $sql = "DROP TABLE IF EXISTS users";

      $this->_BDD->query($sql);

      echo "La table ".self::DB_TABLE_USERS." a été supprimée.";
    }
  }
  
}

