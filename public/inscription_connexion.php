<?php 
session_start();
require 'header.php'; 

$email_valid = "";

function sanitize($string) {
    $words_banned= ["<script>","alert()","</", "</script>", "\\"];
    $result = str_replace($words_banned,"",$string);
    return $result;
}

function email_valid($string) {
    return filter_var($string, FILTER_VALIDATE_EMAIL);
}

if (isset($_POST["username"]) && isset($_POST["email"]) && isset($_POST["password"])) 
{
    $username = sanitize($_POST["username"], "\t\r\n\f\v\-");
    $email = sanitize($_POST["email"]);
    $password = sanitize($_POST["password"]);


    var_dump([$username, $email, $password]);

    if(!email_valid($email)) {
        $email_valid = "invalid";
    } else {
        $email_valid = "valid";
    }
}

if (isset($_SESSION['connect']) && $_SESSION['connect'] === TRUE) {
    header("Location:index.php");
    exit();
}

$_SESSION['connect'] = FALSE;
$error_connection = "";

if (isset($_POST) && !empty($_POST['email']) && !empty($_POST['password'])) {
    require 'repository/Database.php';

    if($password && $email) {
        if($password == $email) {
            $_SESSION['connect'] = TRUE;
            header("Location:index.php");
            exit();
        } else {
            $error_connection = "ERROR";
        } 
    } else {
        $error_connection = "ERROR";
    }
}
?>

<!-- <title>Inscription/Connexion</title> -->


    <div class="main formulaire">
        <div class="inscription">
            <h3>Inscription</h3>
            <form action="#" method="POST">
                Nom d'utilisateur: <br>
                <input type="text" name="username" id="username" required> <br>
                Email : <br>
                <input type="email" name="email" id="email" required> <br>

                <?php
                    if ($email_valid == "invalid") { ?>
                    <div class ="error">
                        <p>Adrresse mail invalide</p>
                    </div>
                <?php } ?>

                Mot de passe : <br>
                <input type="password" name="password" id="password" required> <br>
                <input type="submit" id="submit" value="Soumettre">
            </form>
        </div>
        <div class="connection">
            <h3>Connexion</h3>
            <?php
            if ($error_connection == "ERROR") { ?>
                <div class="error">
                    <p>Login ou mot de passe incorrect</p>
                </div>
            <?php } ?>

            <form action="#" method="POST">
                Email : <br>
                <input type="email" name="email" id="email" required> <br>

                <?php
                    if ($email_valid == "invalid") { ?>
                    <div class ="error">
                        <p>Adrresse mail invalide</p>
                    </div>
                <?php } ?>

                Mot de passe : <br>
                <input type="password" name="password" id="password" required> <br>
                <input type="submit" id="submit" value="Se connecter">
            </form>
        </div>
    </div>  


<?php 
    require 'footer.php'; 
?> 