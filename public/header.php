<?php 


?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="assets/css/style.css" rel="stylesheet" type="text/css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <title>Sur le piste du monde</title>
    <link rel="icon" type="image/x-icon" href="assets/img/logo.png">
</head>
<body>

<header>
    <ul class="menu header">
        <li><a href="article_1">Blog</a></li>
        <li><a href="#">Galerie</a></li>
        <li><a href="index.php"><img class="logo" src="assets/img/logo.png" alt=""></a></li>
        <li><a href="#">Pays</a></li>
        <li><a href="#">Nous</a></li>
        <li><a href="inscription_connexion.php"><img class="user" src="assets/img/user.png" alt=""></a></li>
    </ul>
</header>