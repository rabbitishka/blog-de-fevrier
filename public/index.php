<?php 
require 'header.php'; 
?>

    <div class="container">
    <div class="banner">
        <!-- <img src="/assets/img/banner.svg" alt="nature and montains"> -->
        <h1>Sur le piste du monde</h1>
        <h2>Blog from scratch</h2>
        <div id="button">
            <input type="button" id="apropos" value="A propos de nous">
        </div>
    </div>
    <div class="main">
        <h3>Les articles</h3>
        <div class="articles">
            <div class="article_1">
                <img src="assets/img/girl_and_camel_1.jpg" alt="girl and camel in the desert">
                <p>Lorem ipsum dolor sit amet consectetur. Eros amet purus leo quis posuere arcu pellentesque felis. Volutpat sed ornare id.....</p>
                <a href="article_1.php">
                    <input type="button" class="lire" value="Lire">
                </a>
            </div>
            <div class="article_2">
                <img src="assets/img/man_in_aeroport.jpg" alt="a man in tha airport">
                <p>Lorem ipsum dolor sit amet consectetur. Eros amet purus leo quis posuere arcu pellentesque felis. Volutpat sed ornare id.....</p>
                <a href="article_2.php">
                    <input type="button" class="lire" value="Lire">
                </a>
            </div>
            <div class="article_3">
                <img src="assets/img/van_in_desert.jpg" alt="a van in the desert">
                <p>Lorem ipsum dolor sit amet consectetur. Eros amet purus leo quis posuere arcu pellentesque felis. Volutpat sed ornare id.....</p>
                <a href="article_3.php">
                    <input type="button" class="lire" value="Lire">
                </a>
            </div>
        </div>
    </div>
</div>
    



<?php 
    require 'footer.php'; 
?> 